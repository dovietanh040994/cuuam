@extends('admin.layout.main')
@section('content')
    @include('admin/layout/header')
    <div class="clearfix"></div>
    <div class="page-container">
        @include('admin/layout/sidebar')
        <div class="page-content-wrapper">
            <div class="page-content">

                @include('admin/layout/message')
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="index.html">Cấu hình thời gian sự kiện</a>
                        </li>
                    </ul>
                </div>
{{--                <div class="box-footer clearfix" style="margin-bottom: 20px;margin-top: 10px">--}}

{{--                <a href="{{route('admin.timeSetting.add')}}" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-plus " style="margin-right: 5px"></i>Thêm mới</a>--}}
{{--                </div>--}}
                <!-- /.box-footer -->
                <div class="inbox">
                    <div class="box box-info">
                        <!-- /.box-header -->
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Thời gian bắt đầu</th>
                                    <th>Thời gian kết thúc</th>
                                    <th>Ngày cập nhật</th>
                                    <th>Sửa</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($setting as $key => $value)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$value->start_time}}</td>
                                        <td>{{$value->end_time}}</td>
                                        <td>{{$value->updated_at}}</td>
                                        <td>
                                            <a href="{{route('admin.timeSetting.edit',$value->id)}}" class="btn btn-xs btn-warning" style="margin-top: 10px;">Sửa </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="page-footer-inner">
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
@endsection