@extends('admin.layout.main')
@section('content')
	@include('admin/layout/header')
        <div class="clearfix"></div>
        <div class="page-container">
            @include('admin/layout/sidebar')
            <div class="page-content-wrapper">
                <div class="page-content">
					@include('admin/layout/message')
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="index.html">Product / Add</a>
                            </li>
                        </ul>
                    </div>
                    <div class="inbox">
                        <div class="box">
				            <div class="box-body ">
				                    <!-- Horizontal Form -->
				                    <div class="box box-primary clearfix">
				                        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
				                        	 <input type="hidden" name="_token" value="{{ csrf_token() }}">
					                            <div class="box-body clearfix">
					                                <div class="col-sm-4">
					                                    <div class="form-group">
					                                        <label for="inputEmail3" class="col-sm-2 control-label"> Hình ảnh   </label>
					                                        <div class="col-sm-10">
					                                            <input type="file" class="form-control" name="prd_thunbar" id="imgInp">
					                                            @if($errors->first('prd_thunbar'))
					                                                <span class="text-danger">{{ $errors->first('prd_thunbar') }}</span>
					                                            @endif
					                                        </div>
					                                        <div class="col-sm-10" style="margin-top: 10px;margin-left: 17%">
					                                            <img src="" alt="" class="img img-responsive" id="blah" title=" Logo " style="width: 100%;height: 258px;border: 1px solid #dedede">
					                                        </div>
					                                    </div>

					                                    <div class="form-group">
					                                        <label for="" class="col-sm-2 control-label" style='padding-top: 2px;'> Trạng thái </label>
					                                        <div class="col-sm-10">
					                                        	<div style="display: inline-block;margin-right: 15px;">
					                                        		<input type="radio" name="prd_status" value="1"  checked="" style='opacity: 1;top: 5px;left:17px'>
			                                                    	Hiển thị
					                                        	</div>
			                                                    <div style="display: inline-block;margin-right: 15px;">
			                                                    	<input type="radio" name="prd_status" value="0"   style='opacity: 1;top: 5px;left:17px'>
			                                                    	Ẩn
			                                                    </div>
					                                        </div>
					                                    </div>
{{--														<div class="form-group">--}}
{{--															<label for="inputEmail3" class="col-sm-2 control-label"> Cấu hình thời gian </label>--}}
{{--															<div class="col-sm-10">--}}
{{--																<input type="text" class="form-control" name="setting_time" value="{{ old('setting_time') }}"  placeholder="" autocomplete="off">--}}
{{--																@if($errors->first('setting_time'))--}}
{{--																	<span class="text-danger">{{ $errors->first('setting_time') }}</span>--}}
{{--																@endif--}}
{{--															</div>--}}
{{--														</div>--}}
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-2 control-label"> Lượt mua/ tuần </label>
															<div class="col-sm-10">
																<input type="text" class="form-control" name="turn" value="{{ old('turn') }}"  placeholder="" autocomplete="off">
																@if($errors->first('turn'))
																	<span class="text-danger">{{ $errors->first('turn') }}</span>
																@endif
															</div>
														</div>
{{--														<div class="form-group">--}}
{{--															<label for="" class="col-sm-2 control-label" style='padding-top: 2px;'> Nổi bật </label>--}}
{{--															<div class="col-sm-10">--}}
{{--																<div style="display: inline-block;margin-right: 15px;">--}}
{{--																	<input type="radio" name="prd_hot" value="1"   style='opacity: 1;top: 5px;left:17px'>--}}
{{--																	Bật--}}
{{--																</div>--}}
{{--																<div style="display: inline-block;margin-right: 15px;">--}}
{{--																	<input type="radio" name="prd_hot" value="0" checked=""  style='opacity: 1;top: 5px;left:17px'>--}}
{{--																	Tắt--}}
{{--																</div>--}}
{{--															</div>--}}
{{--														</div>--}}
{{--														<div class="form-group">--}}
{{--															<label for="" class="col-sm-2 control-label" style='padding-top: 2px;'>Hiển thị HOME </label>--}}
{{--															<div class="col-sm-10">--}}
{{--																<div style="display: inline-block;margin-right: 15px;">--}}
{{--																	<input type="radio" name="prd_active" value="1"   style='opacity: 1;top: 5px;left:17px'>--}}
{{--																	Hiển thị--}}
{{--																</div>--}}
{{--																<div style="display: inline-block;margin-right: 15px;">--}}
{{--																	<input type="radio" name="prd_active" value="0" checked=""  style='opacity: 1;top: 5px;left:17px'>--}}
{{--																	Ẩn--}}
{{--																</div>--}}
{{--															</div>--}}
{{--														</div>--}}
					                                </div>
					                                <div class="col-sm-8">
					                                    <div class="form-group">
					                                        <label for="inputEmail3" class="col-sm-2 control-label"> Danh mục </label>
					                                        <div class="col-sm-10">
																<select class="form-control" name="prd_category_product_id" id="">
																	<option value="">-- Chọn --</option>
																	@foreach($category as $key => $value)
																	<option value="{{$value->id}}">{{$value->cpr_name}}</option>
																	@endforeach
																</select>
																@if($errors->first('prd_category_product_id'))
																	<span class="text-danger">{{ $errors->first('prd_category_product_id') }}</span>
																@endif
					                                        </div>
					                                    </div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-2 control-label"> Tiêu đề </label>
															<div class="col-sm-10">
																<input type="text" class="form-control" name="prd_name" value="{{ old('prd_name') }}"  placeholder="" autocomplete="off">
																@if($errors->first('prd_name'))
																	<span class="text-danger">{{ $errors->first('prd_name') }}</span>
																@endif
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-2 control-label"> Mã sản phẩm </label>
															<div class="col-sm-10">
																<input type="text" class="form-control" name="prd_code" value="{{ old('prd_code') }}"  placeholder="" autocomplete="off">
																@if($errors->first('prd_code'))
																	<span class="text-danger">{{ $errors->first('prd_code') }}</span>
																@endif
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-2 control-label"> Coin </label>
															<div class="col-sm-10">
																<input type="text" class="form-control" name="coin" value="{{ old('coin') }}"  placeholder="" autocomplete="off">
																@if($errors->first('coin'))
																	<span class="text-danger">{{ $errors->first('coin') }}</span>
																@endif
															</div>
														</div>
														<div class="form-group">
															<label for="inputEmail3" class="col-sm-2 control-label"> Tỉ lệ vòng quay </label>

															<div class="col-sm-10">
																<input type="text" class="form-control" name="ratioLucky" value="{{ old('ratioLucky') }}"  placeholder="Tỉ lệ vòng quay (%)" autocomplete="off">
																@if($errors->first('ratioLucky'))
																	<span class="text-danger">{{ $errors->first('ratioLucky') }}</span>
																@endif
															</div>
														</div>
					                                    <div class="form-group" >
							                                <label for="inputEmail3" class="col-sm-2 control-label" > Mô tả ngắn </label>
							                                <div class="col-sm-10" >
							                                    <textarea name="prd_description" cols="10" rows="10" class="form-control summernote" placeholder=" Please enter your message">{{ old('prd_description') }}</textarea>
							                                    @if($errors->first('prd_description'))
							                                        <span class="text-danger">{{ $errors->first('prd_description') }}</span>
							                                    @endif
							                                </div>
							                                <div class="clearfix"></div>
							                            </div>
{{--														<div class="form-group" >--}}
{{--															<label for="inputEmail3" class="col-sm-2 control-label" > Thông số máy </label>--}}
{{--															<div class="col-sm-10" >--}}
{{--																<textarea name="prd_info" cols="10" rows="10" class="form-control summernote" placeholder=" Please enter your message">{{ old('prd_info') }}</textarea>--}}
{{--																@if($errors->first('prd_info'))--}}
{{--																	<span class="text-danger">{{ $errors->first('prd_info') }}</span>--}}
{{--																@endif--}}
{{--															</div>--}}
{{--															<div class="clearfix"></div>--}}
{{--														</div>--}}
														<div class="form-group" >
							                                <label for="inputEmail4" class="col-sm-2 control-label" > Nội dung </label>
							                                <div class="col-sm-10" >
							                                    <textarea name="prd_content"  cols="10" rows="10" class="form-control summernote" placeholder=" Please enter your message">{{ old('prd_content') }}</textarea>
							                                    @if($errors->first('prd_content'))
							                                        <span class="text-danger">{{ $errors->first('prd_content') }}</span>
							                                    @endif
							                                </div>
							                                <div class="clearfix"></div>
							                            </div>
					                                    
					                                </div>
					                            </div>
					                            <hr>
					                            <!-- /.box-body -->
					                           <div class="box-footer text-center">
							                        <a href="{{ route('admin.product.index') }}" class="btn btn-danger">Quay lại</a>
							                        <button type="submit" class="btn btn-info">Thêm mới</button>
							                    </div>
					                            <!-- /.box-footer -->
				                        </form>
				                    </div>
				            </div>
				        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-footer">
            <div class="page-footer-inner">
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
@endsection
@section('script')
{{--	<script>--}}
{{--		$(document).ready(function() {--}}
{{--			$('#summernote').summernote({--}}
{{--				placeholder: 'Nội dung',--}}
{{--				tabsize: 2,--}}
{{--				height: 160,--}}
{{--				toolbar: [--}}
{{--					['style', ['bold', 'italic', 'underline', 'clear']],--}}
{{--					['font', ['strikethrough', 'superscript', 'subscript']],--}}
{{--					['fontsize', ['fontsize']],--}}
{{--					['color', ['color']],--}}
{{--					['para', ['ul', 'ol', 'paragraph']],--}}
{{--					['height', ['height']],--}}
{{--					['picture', ['picture']],--}}
{{--				],--}}


{{--			});--}}
{{--		});--}}
{{--		</script>--}}
@endsection