@extends('frontend.layout.main')
@section('script')
    {{--    <script type="text/javascript">--}}
    {{--        // $(document).ready(function(){--}}
    {{--        console.log(111, isEmpty({{Session::has('checkPopupRegister')}}))--}}
    {{--        console.log(222, isEmpty({{Session::has('checkPopupLogin')}}))--}}
    {{--        if (isEmpty({{Session::has('checkPopupRegister')}}) === false) {--}}
    {{--            $('#myModal').modal('show')--}}
    {{--        }--}}
    {{--        if (isEmpty({{Session::has('checkPopupLogin')}}) === false) {--}}
    {{--            $('#myModalLogin').modal('show')--}}
    {{--        }--}}

    {{--        function isEmpty(str) {--}}
    {{--            return (!str || 0 === str.length);--}}
    {{--        }--}}

    {{--        // });--}}
    {{--    </script>--}}
@endsection
@section('content')
    <style>
        .modal-backdrop {
            z-index: 9 !important;
        }
    </style>

    <div class="content">
        <img src="/frontend/images/BANNER2-REVOLUTION.png" alt="" class="img-responsive" style="    width: 989px;
    margin-left: 80px;
    position: absolute;
    top: -469px;">
        {{--        <img src="frontend/images/base/ktc.png" style="position: absolute;top: 84px;width: 1200px;z-index: 9;">--}}
        {{--        <div style="position: absolute;--}}
        {{--    top: 115px;">--}}
        {{--            <img src="/frontend/images/base/ktc.png" style="position: absolute;--}}
        {{--    top: 0;--}}
        {{--    width: 1210px;--}}
        {{--    z-index: 9;--}}
        {{--    left: -38px;--}}
        {{--    height: 530px;">--}}
    </div>
    <img src="frontend/images/base/bg2.png"
         style="position: absolute;width: 1150px;height: 1500px;top: 392px;display: none">
    <div id="sessions-2" style="padding-top: 352px">

        <div class="session-left">
            {{--                    <div class="guide-link">--}}
            {{--                        <div class="guide-blog"><a href="http://cuuam.gosu.vn/cuu-am-lenh" target="_blank"></a></div>--}}
            {{--                        <div class="guide-vip"><a href="http://vip.gosu.vn/" target="_blank"></a></div>--}}
            {{--                        <div class="guide-setup"><a href="cuu-am-bao-dien/tan-thu/co-ban/tai-va-cai-dat-game.html"--}}
            {{--                                                    target="_blank"></a></div>--}}
            {{--                    </div>--}}

            <div class="news-link" style="border-top: 12px solid #00272b;">
                {{--                    <ul class='tabs-news' style="    text-align: center;--}}
                {{--    font-family: 'UVNThanhPho_R';--}}
                {{--    font-size: 36px;--}}
                {{--    color: #fff;">--}}
                {{--                        Kỳ trân các--}}

                {{--                    </ul>--}}
                {{--                        <li class="details_news"><a class="tbnews" href='news/tin-moi2679.html' target="_blank">+</a>--}}
                {{--                        </li>--}}
                <div class="news-content-tab" style="background: #fff">
                    <div class="container" >
                        @if(count($cart) > 0)
                            <a type="button" class="btn btn-danger" style="background: #b70f0b;margin-top: 10px"
                               href="{{route('frontend.cart.order')}}">View Cart ({{count($cart)}})</a>
                        @endif
                        @foreach($postList as $key2 => $item)
                            <div class="clearfix" style="position: relative;    margin-top: 40px;">
                                <p style="    float: left;
    background: antiquewhite;
    padding: 3px 15px;
    z-index: 9;
    position: relative;">{{ $item->cpr_name}}</p>
                                <div style="    width: 100%;
    height: 3px;
    background: red;
    position: absolute;
    bottom: 12px;
    z-index: 8;"></div>
                            </div>
                            <div class="row" style="margin-bottom: 20px;margin-top: 20px;">
                                @foreach($item->product as $key => $value)
                                    <?php
                                    $date = \Carbon\Carbon::now(); // or whatever you're using to set it
                                    $start = $date->copy()->startOfWeek();
                                    $end = $date->copy()->endOfWeek();
                                    $shoppingDay = \App\Models\TransactionHistory::where('user_id', \Illuminate\Support\Facades\Auth::guard('web')->user()->id)
                                        ->where('product_id', $value->id)
                                        ->where('created_at', '>', $start)
                                        ->where('created_at', '<', $end)
                                        ->sum('qty');
                                    $qtyCart = 0;
                                    foreach ($cart as $item) {
                                        if ((int)$item->id === (int)$value->id) {
                                            $qtyCart = $item->qty;
                                        }
                                    }
                                    ?>
                                    <div class="col-md-4">
                                        <div style="border: 1px solid #00272b;    margin-bottom: 20px;">
                                            <div class="boxItem">
                                                <div class="boxProduct clearfix">
                                                    <a href="#" data-toggle="modal"
                                                       data-target="#myProductDetail{{$key2}}{{$key}}">
                                                        <div class="imgProduct">
                                                            <img src="{{asset('/uploads/imgProduct/'.$value->prd_thunbar)}}"
                                                                 alt="">
                                                        </div>
                                                        <div class="contentProduct">{{$value->prd_name}}</div>
                                                        @if($value->turn)
                                                            @if((($value->turn - (int)$shoppingDay) - (int)$qtyCart) > 0)
                                                                <div class="contentProduct">Buy turns/
                                                                    Week: {{(($value->turn - (int)$shoppingDay) - (int)$qtyCart)}}</div>
                                                            @else
                                                                <div class="contentProduct" style="color:red ">Out of Buy turns
                                                                </div>
                                                            @endif
                                                        @endif
                                                    </a>
                                                </div>

                                            </div>
                                            <div class="boxBuy clearfix">
                                                <div class="price">
                                                    {{$value->coin}} <b class="icon-price"></b>
                                                </div>
                                                <div class="buy">
                                                    <a href="{{route('frontend.cart.add',$value->id)}}">
                                                     Buy Now
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal -->
                                        <div class="modal fade" id="myProductDetail{{$key2}}{{$key}}" role="dialog">
                                            <div class="modal-dialog modal-md">
                                                <div class="modal-content">
                                                    <div class="modal-header"
                                                         style="background:url(frontend/images/news-tab.png) no-repeat center center;border-radius: 0;">
                                                        <button type="button" class="close" data-dismiss="modal">&times;
                                                        </button>
                                                        <p style="    position: absolute;
        color: #fff;
        font-family: 'UVNThanhPho_R';
        text-align: center;
        font-size: 21px;
        top: 12px;">{{$value->prd_name}}</p>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>{!! $value->prd_content !!}</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="{{route('frontend.cart.add',$value->id)}}"
                                                           type="button"
                                                           class="btn btn-danger" style="background: #b70f0b">Mua vật
                                                            phẩm</a>
                                                        {{--                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        @endforeach

                        @if($postList->total() > 5)
                            <div style="position: relative;margin-top: 25px;height: 60px;">
                                <div style=" position: absolute;top: 50%; right: 50%;transform: translate(50%,-50%);">
                                    {{$postList->links()}}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>


            </div>
        </div><!-- session-left -->


        <div class="session-right">
            <div class="box-fanpage" style="margin-top: 0;margin-bottom: 7px">
                <div class="title-link">YOUR PROFILE</div>
                <ul class="fanpage-content">
                    <div style="text-align: center;color: #fff;font-family: 'UVNThanhPho_R';font-size: 16px;margin-bottom: 26px;margin-top: 25px;">

						<span style="font-size: 18px">ACCOUNT: {{Auth::user()->userid}}</span> <br>
                        <span style="font-size: 18px">Coin: {{Auth::user()->coin}}<b class="icon-price">
						</b>
						</span>
                    </div>
                </ul>
            </div><!-- END fanpage -->
            <?php

            $topServer = \App\Models\NDV01CharacState::orderBy('inner_level', 'desc')->with('NDV01Charac')->limit(10)->get();
            //                dd($topServer);
            ?>
            {{--                @include('frontend.layout.session-right')--}}
            <a href="https://discord.gg/EAGzzFP5" target="_blank"> <img class="img-responsive" style="width: 100%;margin-bottom: 12px;" src="frontend/images/a6d809ec8e607e3e2771.jpg" alt="asd"></a>
            @include('frontend.layout.combat_record')

           <!--<div>
                <div class="panel panel-default">

                    <div class="title-link">Top 10 Players</div>

                    <ul class="list-group">
                        <li class="list-group-item" style="background: whitesmoke; padding-top: 5px; padding-bottom: 10px;"><h4 style="margin: 0;">Character <span class="pull-right">Level</span></h4></li>
                        @foreach($topServer as $key => $value)
                            <li class="list-group-item">{{$key +1}}. {{$value->NDV01Charac->chr_name}} <span class="pull-right">{{$value->inner_level}}</span></li>
                        @endforeach

                    </ul>
                    <div class="panel-heading text-center"><a href="{{route('frontend.topServer')}}">More</a></div>
                </div>

            </div>
        </div>


    </div><!-- session 2 -->
    </div>


@endsection