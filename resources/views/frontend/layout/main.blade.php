{{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
{{--    <meta charset="utf-8">--}}
{{--    <title>{!! $settingOptions['title'] !!}</title>--}}
{{--    <!-- Stylesheets -->--}}
{{--    <link href="/frontend/css/bootstrap.css" rel="stylesheet">--}}
{{--    <link href="/frontend/css/style.css" rel="stylesheet">--}}
{{--    <link href="/frontend/css/responsive.css" rel="stylesheet">--}}
{{--    <link href="/frontend/css/flaticon.css" rel="stylesheet">--}}
    <link href="/frontend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">

{{--    <link rel="shortcut icon" href="/frontend/images/favicon.png" type="image/x-icon">--}}
{{--    <link rel="icon" href="/frontend/images/favicon.png" type="image/x-icon">--}}

{{--    <!-- Responsive -->--}}
{{--    <meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">--}}
{{--    <!--[if lt IE 9]>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->--}}
{{--    <!--[if lt IE 9]>--}}
{{--    <script src="/frontend/js/respond.js"></script><![endif]-->--}}
{{--    @yield('styles')--}}
{{--</head>--}}

{{--<body>--}}

        <!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"  style="background: #638b6e">

<!-- Mirrored from cuuam.gosu.vn/home/home by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Feb 2018 14:12:49 GMT -->
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="robots" content="index,follow"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> 9D REVOLUTION</title>
    <meta name="description"
          content=" 9D REVOLUTION"/>
    <!--<meta name="keywords" content=""/>-->
    <meta name="keywords"
          content=" 9D REVOLUTION"/>
    <!--<meta property="og:title" content="Cửu Âm Chân Kinh 2 | Kiếm hiệp 3D hardcore chân thực nhất" />-->

    <meta property="og:description"
          content=" 9D REVOLUTION"/>
    <meta name="google-site-verification" content="j5VzirNC2p9Ry58SrTGw9g8cC7I1XWKPNKy1KiYhWu0"/>
{{--    <link href='favicon.ico' rel='icon' type='image/x-icon'/>--}}
{{--    icon-webs--}}
{{--    <link rel="shortcut icon" type="image/png" href="frontend/images/icon-webs.png" id="favicon" data-original-href="frontend/images/icon-webs.png" />--}}
    <link rel="stylesheet" type="text/css"
          href="/cuuam/static/templates/frontend/homepage/cack2/library-js/slick/slick-theme.css">
    <link rel="stylesheet" type="text/css"
          href="/cuuam/static/templates/frontend/homepage/cack2/library-js/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="/cuuam/static/templates/frontend/homepage/cack2/css/style.css">
    <link type="text/css" href="/cuuam/static/templates/frontend/homepage/css/jquery.fancybox.css" rel="stylesheet"/>
    {{--    <link href="cuuam/static.gosu.vn/home/cack/balloon/css/base.css?v=1" rel="stylesheet" type="text/css">--}}
    <link href="/frontend/css/bootstrap.css" rel="stylesheet">
    <link href="{{ URL::asset('admin/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" src="/cuuam/third_party/jquery/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/cuuam/static/templates/frontend/homepage/cack2/js/init.js"></script>
    <link href="/cuuam/static/templates/frontend/homepage/cack2/css/nivo-slider1bce.css?v=6" rel="stylesheet"
          type="text/css">
    <link href="/frontend/vendor/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
    <link href="/frontend/css/style2.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
        .content-footer {
            top: 25px;
        }
        .iconRotationluck {
            width: 120px;
            /*position: fixed;*/
            bottom: 79px;
            right: 47px;
            cursor: pointer;
            z-index: 9999;
        }
        .footer-link {
            height: 30px;
            left: 190px;
            position: absolute;
            top: 4px;
            width: 520px;
        }

        .footer-link ul {
            list-style: none;
        }

        .footer-link li {
            float: left;
            margin-left: 5px;
            color: #000;
        }

        .footer-link li a {
            color: #2e2e2e;
            font-size: 12px;
            font-family: Tahoma;
        }

        .footer-link li a:hover {
            color: #E80812;
        }

        .nav-link {
            padding: 0;
        }
    </style>
    @yield('css')

{{--    <script type="text/javascript">--}}
{{--        <!--//--><![CDATA[//><!----}}
{{--        var pp_gemius_identifier = 'ofHqy.8cY09.vOM8zq6yWJZW7HkF8xBgmofDxyE8a6v.a7';--}}

{{--        // lines below shouldn't be edited--}}
{{--        function gemius_pending(i) {--}}
{{--            window[i] = window[i] || function () {--}}
{{--                var x = window[i + '_pdata'] = window[i + '_pdata'] || [];--}}
{{--                x[x.length] = arguments;--}}
{{--            };--}}
{{--        };gemius_pending('gemius_hit');--}}
{{--        gemius_pending('gemius_event');--}}
{{--        gemius_pending('pp_gemius_hit');--}}
{{--        gemius_pending('pp_gemius_event');--}}
{{--        (function (d, t) {--}}
{{--            try {--}}
{{--                var gt = d.createElement(t), s = d.getElementsByTagName(t)[0],--}}
{{--                    l = 'http' + ((location.protocol == 'home.html') ? 's' : '');--}}
{{--                gt.setAttribute('async', 'async');--}}
{{--                gt.setAttribute('defer', 'defer');--}}
{{--                gt.src = l + '://hc.viam.com.vn/xgemius.js';--}}
{{--                s.parentNode.insertBefore(gt, s);--}}
{{--            } catch (e) {--}}
{{--            }--}}
{{--        })(document, 'script');--}}
{{--        //--><!]]>--}}
{{--    </script>--}}

    <script src="/cuuam/static/templates/frontend/homepage/cack2/library-js/slick/slick.js"></script>

    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/js/easeljs-0.8.1.min.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/js/tweenjs-0.6.1.min.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/js/movieclip-0.8.1.min.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/js/preloadjs-0.6.1.min.js"></script>--}}
    <script type="text/javascript">
        var canvas_url = "/cuuam/static/templates/frontend/homepage/cack2/canvas/index.html";
    </script>
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/Caibang.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/CamYve.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/CoMo.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/CucLacCoc.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/DaoHoaDao.js"></script>--}}

    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/DiHoaCung.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/DuongMon.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/FULLvideo.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/HoaSon.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/HuyetDaoMon.js"></script>--}}

    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/KimChamThamGia.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/LangKhach.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/NgaMi.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/NiemLaba.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/QuanTuDuong.js"></script>--}}

    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/ThanThuyCung.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/TruongPhongTieuCuc.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/TuGiaTrang.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/VanThuSonTrang.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/VoCanMon.js"></script>--}}

    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/Vodang.js"></script>--}}
    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/Taigame.js"></script>--}}

    {{--    <script src="cuuam/static/templates/frontend/homepage/cack2/canvas/canvas.js"></script>--}}

</head>

<body id="body" style="position: relative;">
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v8.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="104110374779913"
     theme_color="#ffc300">
</div>
<div class="wrap-bg">

    @include('frontend.layout.head')

    <div style="">
        @yield('content')
    </div>
</div>
@include('frontend.layout.footer')
<img src="/frontend/images/base/bgd_footernew.png" class="img-responsive" style="width: 100%;">

{{--@endif--}}
{{--@if(Request::segment(1) !== 'lucky-wheel')--}}
{{--<a class="iconRotationluck" href="{{route('frontend.lucky')}}">--}}
{{--    <img src="/luckyWheel.gif" alt="IconRotationluck" style="width: 150px;">--}}
{{--</a>--}}
{{--@endif--}}
<script type="text/javascript" src="/cuuam/third_party/jquery/jquery.gosupopup.min.js"></script>
<script type="text/javascript" src="/cuuam/static/templates/frontend/homepage/js/jquery.fancybox.js"></script>

{{--<script type="text/javascript">--}}
{{--    $(document).ready(function () {--}}
{{--        $(".various").fancybox({--}}
{{--            maxWidth: 719,--}}
{{--            maxHeight: 453,--}}
{{--            fitToView: false,--}}
{{--            width: '719',--}}
{{--            height: '453',--}}
{{--            autoSize: false,--}}
{{--            closeClick: false,--}}
{{--            openEffect: 'none',--}}
{{--            closeEffect: 'none',--}}
{{--            padding: 0--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}

<script type="text/javascript" src="/cuuam/static/templates/frontend/homepage/cack2/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/cuuam/static/templates/frontend/homepage/cack2/js/fadegallery.js"></script>
<script type="text/javascript" src="/cuuam/static/templates/frontend/homepage/cack2/js/common.js"></script>

<script type="text/javascript" src="/cuuam/static.gosu.vn/home/cack/balloon/js/balon-init.js"></script>
<script type="text/javascript" src="/cuuam/static.gosu.vn/home/cack/balloon/js/popup-init.js"></script>

<script type="text/javascript" src="/cuuam/static/templates/frontend/homepage/cack2/js/jquery.nivo.slider.js"></script>
<script src="/frontend/js/bootstrap.min.js"></script>
<script src="/frontend/vendor/toastr/toastr.min.js" type="text/javascript"></script>
@yield('script')

{{--<script type="text/javascript">--}}
{{--    $(window).load(function () {--}}
{{--        $('#slider').nivoSlider();--}}

{{--        $('#owl-monphai').slick({--}}
{{--            dots: false,--}}
{{--            infinite: false,--}}
{{--            speed: 300,--}}
{{--            slidesToShow: 6,--}}
{{--            slidesToScroll: 6,--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}

<!-- popup -->
{{--<script>--}}
{{--    var popup_status = 0;--}}
{{--    $(document).ready(function () {--}}
{{--        if (popup_status == 1) {--}}
{{--            $("#login-box").css("display", "none");--}}
{{--            $("#mask").css("display", "none");--}}
{{--        } else {--}}
{{--            setTimeout(function () {--}}
{{--                if ($('#close').attr('rel') == 1) {--}}
{{--                    FadePopup('a.login-window');--}}
{{--                }--}}
{{--            }, 3000);--}}
{{--        }--}}
{{--    });--}}

{{--</script>--}}
{{--<iframe style="height:0px; width:0px;" src="/cuuam/home/script.html"></iframe>--}}

{{--<!-- Google Analytics -->--}}
{{--<script type="text/javascript">--}}
{{--    var _gaq = _gaq || [];--}}
{{--    _gaq.push(['_setAccount', 'UA-37632868-1']);--}}
{{--    _gaq.push(['_setDomainName', 'gosu.vn']);--}}
{{--    _gaq.push(['_trackPageview']);--}}

{{--    (function () {--}}
{{--        var ga = document.createElement('script');--}}
{{--        ga.type = 'text/javascript';--}}
{{--        ga.async = true;--}}
{{--        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';--}}
{{--        var s = document.getElementsByTagName('script')[0];--}}
{{--        s.parentNode.insertBefore(ga, s);--}}
{{--    })();--}}
{{--</script>--}}
{{--<!-- End Google Analytics -->--}}

{{--<!-- Google Tag Manager -->--}}
{{--<noscript>--}}
{{--    <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-TQMQMM"--}}
{{--            height="0" width="0" style="display:none;visibility:hidden"></iframe>--}}
{{--</noscript>--}}
{{--<script>(function (w, d, s, l, i) {--}}
{{--        w[l] = w[l] || [];--}}
{{--        w[l].push({--}}
{{--            'gtm.start':--}}
{{--                new Date().getTime(), event: 'gtm.js'--}}
{{--        });--}}
{{--        var f = d.getElementsByTagName(s)[0],--}}
{{--            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';--}}
{{--        j.async = true;--}}
{{--        j.src =--}}
{{--            '../../www.googletagmanager.com/gtm5445.html?id=' + i + dl;--}}
{{--        f.parentNode.insertBefore(j, f);--}}
{{--    })(window, document, 'script', 'dataLayer', 'GTM-TQMQMM');</script>--}}
{{--<!-- End Google Tag Manager -->--}}

@include('frontend.message')
</body>

<!-- Mirrored from cuuam.gosu.vn/home/home by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Feb 2018 14:20:39 GMT -->
</html>

{{--<script src="/frontend/js/jquery.js"></script>--}}
{{--<script src="/frontend/js/popper.min.js"></script>--}}
{{--<script src="/frontend/js/jquery-ui.js"></script>--}}
{{--<script src="/frontend/js/jquery.fancybox.js"></script>--}}
{{--<script src="/frontend/js/owl.js"></script>--}}
{{--<script src="/frontend/js/wow.js"></script>--}}
{{--<script src="/frontend/js/appear.js"></script>--}}
{{--<script src="/frontend/js/script.js"></script>--}}
{{--<script src="/frontend/js/xzoom.min.js"></script>--}}
{{--@yield('script')--}}
{{--</body>--}}
{{--</html>--}}