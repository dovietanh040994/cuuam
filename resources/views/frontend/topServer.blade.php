@extends('frontend.layout.main')
@section('script')
    {{--    <script type="text/javascript">--}}
    {{--        // $(document).ready(function(){--}}
    {{--        console.log(111, isEmpty({{Session::has('checkPopupRegister')}}))--}}
    {{--        console.log(222, isEmpty({{Session::has('checkPopupLogin')}}))--}}
    {{--        if (isEmpty({{Session::has('checkPopupRegister')}}) === false) {--}}
    {{--            $('#myModal').modal('show')--}}
    {{--        }--}}
    {{--        if (isEmpty({{Session::has('checkPopupLogin')}}) === false) {--}}
    {{--            $('#myModalLogin').modal('show')--}}
    {{--        }--}}

    {{--        function isEmpty(str) {--}}
    {{--            return (!str || 0 === str.length);--}}
    {{--        }--}}

    {{--        // });--}}
    {{--    </script>--}}
@endsection
@section('css')
    <style type="text/css">
        .content .block {
            width: 100%;
            float: left;
        }

        .content .block h1 {
            width: 100%;
            float: left;
            font-size: 25px;
            font-weight: bold;
            text-indent: 40px;
            color: #34322f;
            text-transform: uppercase;
            background: url(frontend/images/i_h1.png) no-repeat left center;
        }

        .content .block .title {
            width: 160px;
            height: 50px;
            padding-top: 6px;
            margin-left: 20px;
            margin-top: 20px;
            float: left;
            font-size: 25px;
            font-weight: bold;
            text-align: center;
            color: #e9eae7;
            text-transform: uppercase;
            background: url(frontend/images/bg_btn_content.jpg) no-repeat left center;
        }

        .content .block h2 {
            width: 100%;
            float: left;
            font-size: 15px;
            padding-top: 2px;
            margin-top: 10px;
            color: #34322f;
            font-weight: bold;
            text-indent: 20px;
            margin-left: 20px;
            background: url(frontend/images/i_h2.png) no-repeat left center;
        }

        .content .block .col.note {
            color: red;
            text-indent: 20px;
            font-weight: bold;
        }

        .content .block .col {
            width: 100%;
            float: left;
        }

        .content .block .col span {
            width: 50%;
            float: left;
            text-align: left;
        }

        .content .block table {
            width: 100%;
            float: left;
            border-collapse: collapse;
            text-align: center;
            margin-top: 10px;
        }

        .content .block table thead tr {
            background-color: #d7d8d9;
        }

        .content .block table tr {
            border-bottom: 1px solid #d7d8d9;
        }

        .content .block table th, .content .block table td {
            border-left: 1px solid #c3c4c4;
            padding: 10px 0;
        }

        .content .block table tr {
            border-bottom: 1px solid #d7d8d9;
        }

        .content .block table th:first-child, .content .block table td:first-child {
            border: none;
        }

        .content .block table th, .content .block table td {
            border-left: 1px solid #c3c4c4;
            padding: 10px 0;
        }
    </style>
@endsection
@section('content')


    <div class="content">
        <img src="/frontend/images/BANNER2-REVOLUTION.png" alt="" class="img-responsive" style="    width: 989px;
    margin-left: 80px;
    position: absolute;
    top: -469px;">


        <div id="sessions-2">

            <div class="session-left" style="width: 100%;">


                <div class="news-link">
                    <ul class="tabs-news" style="text-align: center;
    font-family: 'UVNThanhPho_R';
    font-size: 36px;
    color: #fff;
    width: 100%;
    background-size: cover;
    margin-left: 0;">
                        Top Players

                    </ul>


                    <div class="news-content-tab" style="background: #fff">
                        <div class="container" style="position: relative;">
                            <div class="block download2 hyhy" style="    padding: 60px 0 91px 0;">
                                <div style="padding: 0 15px">
                                    <table style="border-right: 1px solid #d7d8d9;border-left: 1px solid #d7d8d9;">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Character name</th>
                                            <th>Level</th>
                                            <th>Party</th>
                                            <th>Class</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($topServer as $key => $value)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>{{$value->NDV01Charac->chr_name}}</td>
                                                <td>{{$value->inner_level}}</td>
                                                <td>
                                                    @if($value->NDV01Charac->party == 0)
                                                        Vagabond
                                                    @endif
                                                    @if($value->NDV01Charac->party == 1)
                                                        The league
                                                    @endif
                                                    @if($value->NDV01Charac->party == 2)
                                                        Sacred flower
                                                    @endif
                                                    @if($value->NDV01Charac->party == 3)
                                                        Shao lin
                                                    @endif
                                                    @if($value->NDV01Charac->party == 4)
                                                        The brotherhood
                                                    @endif
                                                    @if($value->NDV01Charac->party == 5)
                                                        Wu Tang
                                                    @endif
                                                    @if($value->NDV01Charac->party == 6)
                                                        Heavenly demon
                                                    @endif
                                                </td>
                                                <td>

                                                    @if($value->NDV01Charac->class == 0)
                                                        Null
                                                    @endif
                                                    @if($value->NDV01Charac->class == 1)
                                                        Tanker
                                                    @endif
                                                    @if($value->NDV01Charac->class == 2)
                                                        Buffer
                                                    @endif
                                                    @if($value->NDV01Charac->class == 3)
                                                        Hybrid
                                                    @endif
                                                    @if($value->NDV01Charac->class == 4)
                                                        Nuker
                                                    @endif
                                                </td>

                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- session-left -->


        </div><!-- session 2 -->
    </div>

@endsection