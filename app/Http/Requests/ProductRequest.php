<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prd_name'        => 'required',
            'prd_thunbar'        => 'required',
            'prd_content'        => 'required',
            'prd_category_product_id'        => 'required',
        ];
    }

    public function messages()
    {
        return [
            'prd_name.required'        => ' Bạn chưa nhập tiêu đề',
            'prd_thunbar.required'        => ' Bạn chưa nhập ảnh',
            'prd_content.required'        => ' Bạn chưa nhập nội dung',
            'prd_category_product_id.required'        => ' Bạn chưa nhập danh mục',
        ];
    }
}
