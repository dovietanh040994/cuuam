<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'b_name'        => 'required',
            'b_thunbar'        => 'required',
            'b_content'        => 'required',
            'b_category_id'        => 'required',
        ];
    }

    public function messages()
    {
        return [
            'b_name.required'        => ' Please enter a title',
            'b_thunbar.required'        => ' Please select an image',
            'b_content.required'        => ' Please enter the content',
            'b_category_id.required'        => ' Please enter the category',
        ];
    }
}
