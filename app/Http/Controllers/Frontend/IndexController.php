<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Blog;
use App\Models\CategoryBlog;
use App\Models\CategoryHelp;
use App\Models\CategoryProduct;
use App\Models\ClientSays;
use App\Models\ConectMomo;
use App\Models\GiftCode;
use App\Models\GiftCodeHistory;
use App\Models\GiftSend;
use App\Models\LuckyNumberSendHistory;
use App\Models\NameGiftCode;
use App\Models\NDV01Charac;
use App\Models\NDV01CharacState;
use App\Models\Product;
use App\Models\Settings;
use App\Models\Tbl_Cash_Inven;
use App\Models\Tbl_Member_Password;
use App\Models\TimeSetting;
use App\Models\TransactionHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Cart;


class IndexController extends Controller
{
    protected $product = '';
    protected $categoryProduct = '';

    public function __construct(Product $product, CategoryProduct $categoryProduct)
    {
        $this->product = $product;
        $this->categoryProduct = $categoryProduct;
    }

    public function index()
    {
        Cart::destroy();

//        Tbl_Member_Password::truncate();
//       dd(Tbl_Member_Password::get(),User::get());
//       dd(Auth::user());

//        $help = CategoryHelp::where('fix',1)->with('help')->limit(4)->first();
//        $productList = $this->product->where('prd_status',1)->where('prd_active',1)->get();

        $categoryBlogList = CategoryBlog::where('cpo_active', 1)->with(['blog' => function ($query) {
            $query->where('b_status', 1);
        }])->get();
        $blog = Blog::where('hot', 1)->limit(10)->get();
//        $listUserNap = TransactionHistory::whereIn('type', [1, 3])->get();
        $listUserNap = User::with(['transactionHistory' => function ($query) {
            $query->whereIn('type', [1, 3]);
        }])->whereHas('transactionHistory')->get();
//        dd($listUserNap);

//        $listUserNap
        $listData = [];
        foreach ($listUserNap as $key => $value ) {
            $total = 0;
            foreach ($value->transactionHistory as $item) {
                $total += $item->coin;
            }
            $listData[$key]['total'] = $total;
            $listData[$key]['userid'] = $value->userid;

        }
        $collection = collect($listData)->SortByDesc('total')->take(10)->values()->all();
        $setting = Settings::where('option_key', 'introduce')->first();

//        dd($collection);
//        $topServer = NDV01CharacState::orderBy('inner_level','desc')->with('NDV01Charac')->limit(10)->get();
        return view('frontend.index', compact('categoryBlogList', 'blog','collection','setting'));
    }

    public function topServer()
    {
        $topServer = NDV01CharacState::orderBy('inner_level', 'desc')->with('NDV01Charac')->limit(100)->get();

        return view('frontend.topServer', compact('topServer'));
    }

    public function download()
    {
        $setting = Settings::where('option_key', 'download')->first();
        return view('frontend.download', compact('setting'));

    }

    public function introduce()
    {
        $setting = Settings::where('option_key', 'introduce')->first();

        return view('frontend.introduce', compact('setting'));

    }

    public function history()
    {
        $showHistory = TransactionHistory::with('product');

        $showHistory = $showHistory->where('user_id', Auth::guard('web')->user()->id)->with('userAdmin')->orderBy('id', 'desc')->paginate(10);
        $dataView = [
            'showHistory' => $showHistory,
        ];
        return view('frontend.history', $dataView);

    }

    public function rechargeCard()
    {
        $arr1 = [
            [
                'id' => 1,
                'vnd' => '50.000',
                'coin' => '50',
            ],
            [
                'id' => 2,
                'vnd' => '100.000',
                'coin' => '100',
            ],
            [
                'id' => 3,
                'vnd' => '200.000',
                'coin' => '200',
            ],
            [
                'id' => 4,
                'vnd' => '500.000',
                'coin' => '500',
            ],

        ];
        $arr2 = [
            [
                'id' => 5,
                'vnd' => '1.000.000',
                'coin' => '1000',
            ],
            [
                'id' => 6,
                'vnd' => '2.000.000',
                'coin' => '2000',
            ],
            [
                'id' => 7,
                'vnd' => '5.000.000',
                'coin' => '5000',
            ],
            [
                'id' => 8,
                'vnd' => '10.000.000',
                'coin' => '10000',
            ],
        ];
        return view('frontend.recharge_card', compact('arr1', 'arr2'));

    }

    public function rechargeCardPost(Request $request)
    {
        $total = ConectMomo::where('id', 1)->value('total');
        $updatedTime = ConectMomo::where('id', 1)->value('updated_at');
        if ((Carbon::parse($updatedTime)->addMinute(1) >= Carbon::now())) {
            if ($total < 2) {
                ConectMomo::where('id', 1)->update(['total' => 2]);
            } else {
                return redirect()->back()->with(['alert' => 'warning', 'message' => 'Hệ thống đang quá tải, mời bạn chờ trong 1 phút']);

            }
        } else {
            ConectMomo::where('id', 1)->update(['total' => 1]);
        }


        $myBody = [
            'phone' => "0865633199",
            'begin' => (string)Carbon::now()->subDay(7)->timestamp . '000',
            'end' => (string)Carbon::now()->timestamp . '000',
        ];
//        http://9dggate.com/
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'http://103.130.214.10:1234/getTransactions',
            [
                \GuzzleHttp\RequestOptions::JSON => $myBody
            ]
        );
        $data = json_decode($response->getBody()->getContents());
        $amount = '';
        $transId = '';
        $from = '';
        if ($data->message === 'ok' && count($data->shortTrans) > 0) {
            foreach ($data->shortTrans as $item) {
                if (($item->transId == $request->momoCode) && ($item->from == $request->phone)) {
                    $checkCode = TransactionHistory::where('code', $request->momoCode)->first();
                    if (!empty($checkCode)) {
                        return redirect()->back()->with(['alert' => 'warning', 'message' => 'Mã giao dịch đã được sử dụng']);
                    }
                    $amount = $item->amount;
                    $transId = $item->transId;
                    $from = $item->from;
                }
            }
        } else if ($data->message === 'ok' && count($data->shortTrans) === 0) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'Không tìm thấy giao dịch, báo lại Admin để được hỗ trợ']);
        }

        if ((int)$amount < 1000) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'Số tiền giao dịch không được chấp nhận']);
        }
        if (!empty($amount) && !empty($transId) && !empty($from)) {
            $checkX2 = TransactionHistory::where('user_id', Auth::guard('web')->user()->id)->first();
            $totalCoin = 0;
            if (!empty($checkX2)) {
                $totalCoin = (int)($amount / 1000);
                $timeSetting = TimeSetting::where('id', 1)->where('status', 1)->first();
                if ($timeSetting) {
                    if (Carbon::now() > Carbon::parse($timeSetting->start_time) && Carbon::now() < Carbon::parse($timeSetting->end_time)) {

                        $checkPackage = TransactionHistory::where('user_id', Auth::guard('web')->user()->id)->where('package_id', $request->packageId)->first();
                        if (!empty($checkPackage)) {
                            $totalCoin = (int)(($amount / 1000));
                        } else {
                            $totalCoin = (int)(($amount / 1000) * 2);
                        }


                    }
                }
            } else {
                $totalCoin = (int)(($amount / 1000) * 2);
            }
            $arr = [
                'user_id' => Auth::guard('web')->user()->id,
                'coin' => $totalCoin,
                'code' => $transId,
                'phone' => $from,
                'package_id' => $request->packageId,
                'type' => 1,
            ];
            TransactionHistory::insert($arr);

            User::where('id', Auth::guard('web')->user()->id)->increment('coin', $totalCoin);

            ///kkkkkkkk

            $listUserNap = User::where('id', Auth::guard('web')->user()->id)->with(['transactionHistory' => function ($query) {
                $query->whereIn('type', [1, 3]);
            }])->first();
            $total = 0;
            foreach ($listUserNap->transactionHistory as $value) {
                $total += $value->coin;
            }

            if ($total >= 100) {
                $product = GiftSend::where('giftCoin', 100)->first();
                $check = LuckyNumberSendHistory::where('user_id', Auth::guard('web')->user()->id)->where('gift', 100)->first();
                if (!$check) {
                    $dataImport = [
                        'turn' => $product->luckyNumber,
                        'gift' => 100,
                        'user_id' => Auth::guard('web')->user()->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                    LuckyNumberSendHistory::insert($dataImport);

                    User::where('id', Auth::guard('web')->user()->id)->increment('luckyNumber', (int)$product->luckyNumber);
                }
            }
            if ($total >= 200) {
                $product = GiftSend::where('giftCoin', 200)->first();
                $check = LuckyNumberSendHistory::where('user_id', Auth::guard('web')->user()->id)->where('gift', 200)->first();
                if (!$check) {
                    $dataImport = [
                        'turn' => $product->luckyNumber,
                        'gift' => 200,
                        'user_id' => Auth::guard('web')->user()->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                    LuckyNumberSendHistory::insert($dataImport);
                    User::where('id', Auth::guard('web')->user()->id)->increment('luckyNumber', (int)$product->luckyNumber);

                }
            }
            if ($total >= 500) {
                $product = GiftSend::where('giftCoin', 500)->first();
                $check = LuckyNumberSendHistory::where('user_id', Auth::guard('web')->user()->id)->where('gift', 500)->first();
                if (!$check) {
                    $dataImport = [
                        'turn' => $product->luckyNumber,
                        'gift' => 500,
                        'user_id' => Auth::guard('web')->user()->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                    LuckyNumberSendHistory::insert($dataImport);
                    User::where('id', Auth::guard('web')->user()->id)->increment('luckyNumber', (int)$product->luckyNumber);

                }
            }
            if ($total >= 1000) {
                $product = GiftSend::where('giftCoin', 1000)->first();
                $check = LuckyNumberSendHistory::where('user_id', Auth::guard('web')->user()->id)->where('gift', 1000)->first();
                if (!$check) {
                    $dataImport = [
                        'turn' => $product->luckyNumber,
                        'gift' => 1000,
                        'user_id' => Auth::guard('web')->user()->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                    LuckyNumberSendHistory::insert($dataImport);
                    User::where('id', Auth::guard('web')->user()->id)->increment('luckyNumber', (int)$product->luckyNumber);

                }
            }
            if ($total >= 2000) {
                $product = GiftSend::where('giftCoin', 2000)->first();
                $check = LuckyNumberSendHistory::where('user_id', Auth::guard('web')->user()->id)->where('gift', 2000)->first();
                if (!$check) {
                    $dataImport = [
                        'turn' => $product->luckyNumber,
                        'gift' => 2000,
                        'user_id' => Auth::guard('web')->user()->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                    LuckyNumberSendHistory::insert($dataImport);
                    User::where('id', Auth::guard('web')->user()->id)->increment('luckyNumber', (int)$product->luckyNumber);

                }
            }
            if ($total >= 5000) {
                $product = GiftSend::where('giftCoin', 5000)->first();
                $check = LuckyNumberSendHistory::where('user_id', Auth::guard('web')->user()->id)->where('gift', 5000)->first();
                if (!$check) {
                    $dataImport = [
                        'turn' => $product->luckyNumber,
                        'gift' => 5000,
                        'user_id' => Auth::guard('web')->user()->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                    LuckyNumberSendHistory::insert($dataImport);
                    User::where('id', Auth::guard('web')->user()->id)->increment('luckyNumber', (int)$product->luckyNumber);

                }
            }
            if ($total >= 10000) {
                $product = GiftSend::where('giftCoin', 10000)->first();
                $check = LuckyNumberSendHistory::where('user_id', Auth::guard('web')->user()->id)->where('gift', 10000)->first();
                if (!$check) {
                    $dataImport = [
                        'turn' => $product->luckyNumber,
                        'gift' => 10000,
                        'user_id' => Auth::guard('web')->user()->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                    LuckyNumberSendHistory::insert($dataImport);
                    User::where('id', Auth::guard('web')->user()->id)->increment('luckyNumber', (int)$product->luckyNumber);

                }
            }
            return redirect()->back()->with(['alert' => 'success', 'message' => 'Nạp thẻ thành công']);

        } else {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'Số điện thoại hoặc mã giao dịch không đúng']);
        }

    }

    public function giftCode(Request $request)
    {
//        dd(Tbl_Cash_Inven::orderBy('order_idx', 'desc')->limit(10)->get());
        $nameCode = NameGiftCode::where('name', $request->code)->first();
        if (empty($nameCode)) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'Gift Code không tồn tại']);
        }


        $giftCode = GiftCode::where('id', $nameCode->gift_code_id)->where('status', 1)->first();

        if (!empty($giftCode)) {

            if ($giftCode->type === 1) {
                $checkCode = GiftCodeHistory::where('user_id', '!=', Auth::guard('web')->user()->id)->where('gift_code_name_id', $nameCode->id)->get();

                if (count($checkCode) > 0) {
                    return redirect()->back()->with(['alert' => 'warning', 'message' => 'Gift Code này đã được sử dụng']);
                }
                $checkGiftCodeId = NameGiftCode::where('id', $nameCode->id)->value('gift_code_id');
                $showHistory = GiftCodeHistory::where('user_id', Auth::guard('web')->user()->id)->where('gift_code_name_id', '!=', $nameCode->id)->with(['giftCodeName' => function ($query) use ($checkGiftCodeId) {
                    $query->with(['giftCode' => function ($query2) use ($checkGiftCodeId) {
                        $query2->where('id', $checkGiftCodeId);
                    }]);
                }])->get();
                foreach ($showHistory as $value) {
                    if ($value->giftCodeName->id === $checkGiftCodeId) {
                        return redirect()->back()->with(['alert' => 'warning', 'message' => 'Gift Code này đã được sử dụng 11']);
                    }
                }
            }

            $checkUser = GiftCodeHistory::where('user_id', Auth::guard('web')->user()->id)->where('gift_code_name_id', $nameCode->id)->get();

            if (count($checkUser) == $giftCode->qty) {
                return redirect()->back()->with(['alert' => 'warning', 'message' => 'Tài khoản đã sử dụng hết Gift Code này']);
            }
//            dd(  [
//                'gift_code_name_id' => $nameCode->id,
//                'user_id' => Auth::guard('web')->user()->id,
//            ], [
//                'order_idx' => Tbl_Cash_Inven::max('order_idx') + 1,
//                'item_code' => $giftCode->gift_code,
//                'item_user_id' => Auth::guard('web')->user()->userid,
//                'item_server_code' => 0,
//                'item_present' => 0,
//                'order_input_date' => Carbon::now(),
//            ]);


            GiftCodeHistory::insert(
                [
                    'gift_code_name_id' => $nameCode->id,
                    'user_id' => Auth::guard('web')->user()->id,
                ]
            );

            Tbl_Cash_Inven::insert(
                [
                    'order_idx' => Tbl_Cash_Inven::max('order_idx') + 1,
                    'item_code' => $giftCode->gift_code,
                    'item_user_id' => Auth::guard('web')->user()->userid,
                    'item_server_code' => 0,
                    'item_present' => 0,
                    'order_input_date' => Carbon::now(),
                ]
            );
            return redirect()->back()->with(['alert' => 'success', 'message' => 'Nhập Gift Code thành công']);

        } else {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'Gift Code đã hết hạn']);
        }


    }
}
