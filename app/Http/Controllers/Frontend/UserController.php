<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Tbl_Member_Password;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use Cart;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $rules = array(
            'u_name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        );

        $validator = array(
            'u_name.required' => 'Please enter your account',
            'email.required' => 'Email cannot be empty',
            'email.email' => 'Email invalidate',
            'password.required' => 'Please enter a password',
            'password_confirmation.required' => 'Please enter a password',
            'password_confirmation.same' => 'The password 2 is not correct',
        );
        Session::put('checkPopupRegister', 1);
        $this->validate($request, $rules, $validator);

        $checkEmail = User::where('email',$request->input('email'))->first();
        if ($checkEmail) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'Email already exists']);

        }
//        dd($checkEmail);
        $checkDuplicase = Tbl_Member_Password::where('userid', $request->input('u_name'))->first();
        if ($checkDuplicase) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'This account has already existed']);

        }

        Session::forget('checkPopupRegister');


//        $hashed_password = password_hash($request->input('password'), PASSWORD_DEFAULT);
        $hashed_password = md5($request->input('password'));

        $users2 = new User();
        $users2->userid = $request->input('u_name');
        $users2->email = $request->input('email');
        $users2->userpassword = $hashed_password;
        $users2->save();
        Auth::login($users2, true);

        $users = new Tbl_Member_Password();
        $users->userid = $request->input('u_name');
        $users->userpassword = $hashed_password;
//        $users->paasen = null;
        $users->save();

        return redirect()->back()->with(['alert' => 'success', 'message' => 'Create an account and login successfully']);

    }

    public function login(Request $request)
    {
//        dd( Auth::guard('web')->user());
        Cart::destroy();
        $rules = array(
            'u_name' => 'required',
            'password' => 'required',
        );

        $validator = array(
            'u_name.required' => 'Please enter your account',
            'password.required' => 'Please enter your password',
        );
        Session::put('checkPopupLogin', 1);
        $this->validate($request, $rules, $validator);
        $user = User::where('userid', $request->u_name)
            ->where('userpassword', md5($request->password))
            ->first();
//        dd($user);
        if ($user) {
            Session::forget('checkPopupLogin');
            Auth::login($user, true);

            return redirect()->back()->with(['alert' => 'success', 'message' => 'Logged in successfully']);
        } else {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'The account or password something wrong !']);
        }

    }

    public function loginHome(Request $request)
    {
        $rules = array(
            'u_name' => 'required',
            'password' => 'required',
        );

        $validator = array(
            'u_name.required' => 'Please enter your account',
            'password.required' => 'Please enter your password',
        );
        $this->validate($request, $rules, $validator);
        $user = User::where('userid', $request->u_name)
            ->where('userpassword', md5($request->password))
            ->first();
        if ($user) {
            Auth::login($user, true);

            return redirect()->back()->with(['alert' => 'success', 'message' => 'Logged in successfully']);
        } else {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'The account or password something wrong !']);
        }

    }



    public function backResetPass()
    {
        Session::forget('checkResetPass');

        return redirect()->back();
    }
    public function putResetPass()
    {
        Session::put('checkResetPass', 1);

        return redirect()->back();
    }
    public function resetPass(Request $request)
    {

        $rules = array(
            'u_name' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        );

        $validator = array(
            'u_name.required' => 'Please enter your account',
            'password.required' => 'Please enter your new password',
            'password_confirmation.required' => 'Please re-enter your new password',
            'password_confirmation.same' => 'The password is not correct',
        );
        $this->validate($request, $rules, $validator);
//        $checkDuplicase = Tbl_Member_Password::where('userid', $request->input('u_name'))->first();
        if (Auth::user()->userid !== $request->input('u_name')) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'The account name is not the same as the login account']);

        }

        Session::forget('checkResetPass');

        $hashed_password = md5($request->input('password'));
        User::where('userid', $request->input('u_name'))->update(['userpassword' => $hashed_password]);
        Tbl_Member_Password::where('userid', $request->input('u_name'))->update(['userpassword' => $hashed_password]);
        Auth::guard('web')->logout();
        Cart::destroy();

        return redirect()->back()->with(['alert' => 'success', 'message' => 'Password successfully changed, please login again']);
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        Cart::destroy();

        return redirect()->back()->with(['alert' => 'success', 'message' => 'Account logout is successful']);
    }
}
