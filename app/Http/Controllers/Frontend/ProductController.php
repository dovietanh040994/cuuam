<?php

namespace App\Http\Controllers\Frontend;

use App\Models\CategoryProduct;
use App\Models\Product;
use App\Models\Tbl_Cash_Inven;
use App\Models\TransactionHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use Auth;

class ProductController extends Controller
{
    protected $product = '';
    protected $categoryProduct = '';

    public function __construct(Product $product, CategoryProduct $categoryProduct)
    {
        $this->product = $product;
        $this->categoryProduct = $categoryProduct;
    }

//    public function product()
//    {
//        $showListProduct = $this->product->where('b_status',1)->orderBy('id', 'desc')->paginate(8);
//        $dataView = [
//            'showListProduct' => $showListProduct,
//        ];
//        return view('frontend.product',$dataView);
//    }
    public function search(Request $request)
    {

        $showproduct = Product::where('prd_name', 'like', '%' . $request->name . '%')
            ->with(['categoryProduct' => function ($query) {
                $query->where('cpr_active', 1);
            }])->orderBy('id', 'desc')->paginate(10);

        $dataView = [
            'showproduct' => $showproduct,
            'query' => $request->query()
        ];
        return view('frontend.search', $dataView);
    }

    public function productDetail($id)
    {
        $productDetail = $this->product->where('id', $id)->first();
        $productHot = $this->product->where('prd_status', 1)->where('prd_hot', 1)->get();
        $productList = $this->product->where('prd_status', 1)->where('prd_category_product_id', $productDetail->prd_category_product_id)->limit(6)->get();
        return view('frontend.product_detail', compact('productDetail', 'productHot', 'productList'));
    }

    public function productCate()
    {
//        $postList = $this->product->where('prd_status', 1)->paginate(18);
        $postList = $this->categoryProduct->with(['product' => function ($query) {
            $query->where('prd_status', 1);
        }])->paginate(5);
//        dd($postList);
        $cart = Cart::content();
//        dd($cart);
        return view('frontend.productList', compact('postList', 'cart'));
    }


    public function addCart(Request $request, $id)
    {
//        return redirect()->back()->with(['alert' => 'warning', 'message' => 'Tạm thời khóa chức năng mua bán, mời bạn liên hệ với Admin']);
        $product = $this->product->where('id', $id)->first();

        $date = Carbon::now(); // or whatever you're using to set it
        $start = $date->copy()->startOfWeek();
        $end = $date->copy()->endOfWeek();
        $shoppingDay = TransactionHistory::where('user_id', Auth::guard('web')->user()->id)
            ->where('product_id', $id)
            ->where('created_at', '>', $start)
            ->where('created_at', '<', $end)
            ->sum('qty');

        if ($product->turn) {
            if ((int)$product->turn <= (int)$shoppingDay) {
                return redirect()->back()->with(['alert' => 'warning', 'message' => 'The items is out of purchase the week']);
            }

            $cart = Cart::content();
            $qtyCart = 0;
            foreach ($cart as $item) {
                if ((int)$item->id === (int)$id) {
                    $qtyCart = $item->qty;
                }
            }

            if ((($product->turn - (int)$shoppingDay) - (int)$qtyCart) == 0) {
                return redirect()->back()->with(['alert' => 'warning', 'message' => 'Out of buy turns']);
            }
        }
//dd(['id' => $id, 'name' => $product->prd_name, 'qty' => 1, 'price' => $product->coin, 'options' => ['image' => $product->prd_thunbar, 'prd_code' => $product->prd_code]])
//        Cart::add(['id' =>$id]);
        Cart::add(['id' => $id, 'name' => $product->prd_name, 'qty' => 1, 'price' => $product->coin, 'options' => ['image' => $product->prd_thunbar, 'prd_code' => $product->prd_code]]);


//        Cart::add($product);
        return redirect()->back()->with(['alert' => 'success', 'message' => 'The items has been added to cart']);
    }

    public function delete($id)
    {
        Cart::remove($id);
        return redirect()->back()->with(['alert' => 'success', 'message' => 'The items has been successfully deleted']);
    }

    public function increment($productId)
    {

        $product = $this->product->where('id', $productId)->first();

        $date = Carbon::now(); // or whatever you're using to set it
        $start = $date->copy()->startOfWeek();
        $end = $date->copy()->endOfWeek();
        $shoppingDay = TransactionHistory::where('user_id', Auth::guard('web')->user()->id)
            ->where('product_id', $productId)
            ->where('created_at', '>', $start)
            ->where('created_at', '<', $end)
            ->sum('qty');
        if ($product->turn) {
            if ($product->turn <= (int)$shoppingDay) {
                return redirect()->back()->with(['alert' => 'warning', 'message' => 'The items is out of purchase the week']);
            }

            $cart = Cart::content();
            $qtyCart = 0;
            foreach ($cart as $item) {
                if ((int)$item->id === (int)$productId) {
                    $qtyCart = $item->qty;
                }
            }

            if ((($product->turn - (int)$shoppingDay) - (int)$qtyCart) == 0) {
                return redirect()->back()->with(['alert' => 'warning', 'message' => 'The items is out of purchase the week']);
            }
        }

        $rows = Cart::search(function ($key, $value) use ($productId) {
            return $key->id == $productId;
        });
        $item = $rows->first();
        Cart::update($item->rowId, $item->qty + 1);

        return redirect()->back()->with(['alert' => 'success', 'message' => 'Update successfully']);
    }

    public function decrease($productId)
    {
        $rows = Cart::search(function ($key, $value) use ($productId) {
            return $key->id == $productId;
        });
        $item = $rows->first();
        Cart::update($item->rowId, $item->qty - 1);
        return redirect()->back()->with(['alert' => 'success', 'message' => 'Update successfully']);
    }

    public function order()
    {
        $cart = Cart::content();
        return view('frontend.order', compact('cart'));

    }

    public function payment()
    {
//        return redirect()->back()->with(['alert' => 'warning', 'message' => 'Bạn Liên hệ với admin để mua vật phẩm']);

//dd("No No :)");
//        if($date->eq($start)) {
//            // do your formatting here
//        }
//        dd(Tbl_Cash_Inven::orderByDesc('order_idx')->limit(10)->get());
        $cart = Cart::content();
        $total = 0;
        foreach ($cart as $item) {
            $coin = Product::where('id', $item->id)->value('coin');
            $total += $item->qty * (int)$coin;
        }


        if ($total === 0) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'You have not selected an item']);
        }


        $user = User::where('id', Auth::guard('web')->user()->id)->first();
        if ((int)$total > (int)$user->coin) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'You do not have enough coins']);
        }

//        dd($cart);
        if ((int)$user->coin >= (int)$total) {
//            $checkUpdate = User::where('id', Auth::guard('web')->user()->id)->decrement('coin', (int)$total);
            $coinChuaTru = (int)$user->coin;
            $checkUpdate = User::where('id', Auth::guard('web')->user()->id)->update(['coin' =>  (int)$user->coin - (int)$total]);
            $coinDaTru = User::where('id', Auth::guard('web')->user()->id)->first()->coin;
            if ($checkUpdate === 1 && ((int)$coinChuaTru - (int)$coinDaTru) == (int)$total) {
                $arr = [];
                foreach ($cart as $item) {
                    $soTienHienTai = User::where('id', Auth::guard('web')->user()->id)->value('coin');
//                    $soTienLanGiaoDichCuoiCung = TransactionHistory::where('user_id', Auth::guard('web')->user()->id)->orderBy('id', 'desc')->first();
////                    dd($soTienHienTai, $soTienLanGiaoDichCuoiCung);
//                    if(!empty($soTienLanGiaoDichCuoiCung) && $soTienHienTai == $soTienLanGiaoDichCuoiCung->coin){
//                        return redirect()->back()->with(['alert' => 'warning', 'message' => 'Chúc mừng bạn đã Hack thành công :)']);
//
//                    }
                    array_push($arr, [
                        'product_id' => $item->id,
                        'user_id' => Auth::guard('web')->user()->id,
                        'coin' => (int)($item->qty * $item->price),
                        'qty' => $item->qty,
                        'remaining_amount' => $soTienHienTai,
                        'type' => 2,
                    ]);

                    if ($item->qty > 1) {
                        for ($x = 1; $x <= $item->qty; $x++) {
                            Tbl_Cash_Inven::insert(
                                [
                                    'order_idx' => Tbl_Cash_Inven::max('order_idx') + 1,
                                    'item_code' => $item->options->prd_code,
                                    'item_user_id' => Auth::guard('web')->user()->userid,
                                    'item_server_code' => 0,
                                    'item_present' => 0,
                                    'order_input_date' => Carbon::now(),
                                ]
                            );
                        }
                    } else {
                        Tbl_Cash_Inven::insert(
                            [
                                'order_idx' => Tbl_Cash_Inven::max('order_idx') + 1,
                                'item_code' => $item->options->prd_code,
                                'item_user_id' => Auth::guard('web')->user()->userid,
                                'item_server_code' => 0,
                                'item_present' => 0,
                                'order_input_date' => Carbon::now(),
                            ]
                        );
                    }
                }
                TransactionHistory::insert($arr);
                Cart::destroy();
                return redirect()->route('frontend.productCate')->with(['alert' => 'success', 'message' => 'Payment success']);
            }else{
                return redirect()->back()->with(['alert' => 'warning', 'message' => 'Payment failed']);
            }
        }else{
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'no no no :)']);

        }


    }


}
