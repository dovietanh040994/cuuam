<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Blog;
use App\Models\CategoryBlog;
use App\Models\CategoryHelp;
use App\Models\Help;
use App\Models\PasswordReset;
use App\Models\Product;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class PasswordResetController extends Controller
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */

    public function getSendMail()
    {
//        Session::put('checkResetPass', 1);
//        return redirect()->back();

        return view('frontend.sendMail');

    }

    public function create(Request $request)
    {
//        dd(1);

        $rules = array(
            'email' => 'required|email',
        );

        $validator = array(
            'email.required' => 'Email cannot be empty',
            'email.email' => 'Email invalidate',
        );
        $this->validate($request, $rules, $validator);
        $checkLimitMail = User::where('email', $request->email)->get();
        if(count($checkLimitMail) > 1){
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'Email already exists']);

        }
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'We couldnt find the user with that e-mail address']);
        }
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => str_random(60)
            ]
        );
        if ($user && $passwordReset) {
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
        }

        return redirect()->back()->with(['alert' => 'success', 'message' => 'We have send a email reset password link , please check your mailbox or draft spam mailbox']);

    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        $mess = 'null';
        if (!$passwordReset) {
            $mess = 'This password reset token is not valid.';
            return view('frontend.resetPass', compact('mess', 'token'));
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            $mess = 'This password reset token is not valid.';
            return view('frontend.resetPass', compact('mess', 'token'));
        }
        return view('frontend.resetPass', compact('mess', 'token'));

    }

    /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        );

        $validator = array(
            'email.required' => 'Please enter Email',
            'email.email' => 'Email invalidate',
            'password.required' => 'Please enter a new password',
            'password_confirmation.required' => 'Please enter a new password',
            'password_confirmation.same' => 'The password is not correct',
        );
        $this->validate($request, $rules, $validator);
        $passwordReset = PasswordReset::where([
            ['token', $request->tokenMail],
            ['email', $request->email]
        ])->first();

        if (!$passwordReset) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'This password reset token is not valid.']);

        }


        $user = User::where('email', $passwordReset->email)->first();
        if (!$user) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'We couldnt find the user with that e-mail address.']);

        }


        $user->userpassword = md5($request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));

        Auth::guard('web')->logout();
        return redirect()->route('frontend.index')->with(['alert' => 'success', 'message' => 'Password changed successfully']);

//        return response()->json($user);
    }
}
