<?php

/**
 * PAYPAL API SERVICE TEST
 */

namespace App\Http\Controllers\Frontend;


use App\Models\TimeSetting;
use App\Models\TransactionHistory;
use App\User;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\PayPalService as PayPalSvc;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Config;


class PayPalTestController extends Controller
{

    private $paypalSvc;

    public function __construct(PayPalSvc $paypalSvc)
    {
        parent::__construct();

        $this->paypalSvc = $paypalSvc;
    }

    public function index(Request $request)
    {
        if(!empty($request->coin)){
            $coin = 0;
            if ($request->coin == 1) {
                $coin = 1000;
            } elseif ($request->coin == 2) {
                $coin = 2000;
            }elseif ($request->coin == 3) {
                $coin = 3000;
            }elseif ($request->coin == 4) {
                $coin = 5000;
            }elseif ($request->coin == 5) {
                $coin = 10000;
            }
            $data = [
                [
                    'name' => $coin . ' Coin',
                    'quantity' => 1,
                    'price' => $coin / 100,
//                'sku' => '1'
                ]
            ];





            $transactionDescription = "Tobaco";

            $paypalCheckoutUrl = $this->paypalSvc
                // ->setCurrency('eur')
                ->setReturnUrl(route('getPaymentStatus'))
                // ->setCancelUrl(url('paypal/status'))
                ->setItem($data)
                // ->setItem($data[0])
                // ->setItem($data[1])
                ->createPayment($transactionDescription);

            if ($paypalCheckoutUrl) {
                return redirect($paypalCheckoutUrl);
            } else {
                dd(['Error']);
            }
        }else{
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'Bạn chưa chọn mệnh giá']);
        }

    }

    public function status()
    {
        $paymentStatus = $this->paypalSvc->getPaymentStatus();
//        dd($paymentStatus->transactions[0]->currency === 'USD');
//        dd($paymentStatus->transactions[0]->amount->total);
        $coin = (int)$paymentStatus->transactions[0]->amount->total * 100;

        $packageId = 0;
        if ($coin ==  1000 ) {
            $packageId = 1;
        } elseif ($coin == 2000 ) {
            $packageId = 2;
        }elseif ($coin == 3000 ) {
            $packageId = 3;
        }elseif ($coin == 5000 ) {
            $packageId = 4;
        }elseif ($coin == 10000 ) {
            $packageId = 5;
        }
//        $totalCoin = $coin;
//        $timeSetting = TimeSetting::where('id', 1)->where('status', 1)->first();
//        if ($timeSetting) {
//            if (Carbon::now() > Carbon::parse($timeSetting->start_time) && Carbon::now() < Carbon::parse($timeSetting->end_time)) {
//                $totalCoin = (int)($coin * 2);
//            }
//        }


        $checkX2 = TransactionHistory::where('user_id', Auth::guard('web')->user()->id)->first();
        $totalCoin = 0;
        $timeSetting = TimeSetting::where('id', 1)->where('status', 1)->first();

        if (!empty($checkX2)) {
            $totalCoin = (int)($coin);
            if ($timeSetting) {
                if (Carbon::now() > Carbon::parse($timeSetting->start_time) && Carbon::now() < Carbon::parse($timeSetting->end_time)) {
                    $checkPackage = TransactionHistory::where('user_id', Auth::guard('web')->user()->id)->where('number_time_setting', $timeSetting->number)->where('package_id', $packageId)->first();
                    if (!empty($checkPackage)) {
                        $totalCoin = (int)(($coin));
                    } else {
                        $totalCoin = (int)(($coin) * Config::get('base.promotion'));
                    }


                }
            }
        } else {
            $totalCoin = (int)(($coin) * 2);
        }
        if ($timeSetting && (Carbon::now() > Carbon::parse($timeSetting->start_time) && Carbon::now() < Carbon::parse($timeSetting->end_time))) {
            $arr = [
                'user_id' => Auth::guard('web')->user()->id,
                'coin' => $totalCoin,
                'package_id' => $packageId,
                'number_time_setting' => $timeSetting->number,
                'type' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }else{
            $arr = [
                'user_id' => Auth::guard('web')->user()->id,
                'coin' => $totalCoin,
                'type' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }

        TransactionHistory::insert($arr);


        User::where('id', Auth::guard('web')->user()->id)->increment('coin', (int)$totalCoin);
        return redirect()->route('frontend.index')->with(['alert' => 'success', 'message' => 'Payment success']);
    }

    public function paymentList()
    {
        $limit = 10;
        $offset = 0;

        $paymentList = $this->paypalSvc->getPaymentList($limit, $offset);

        dd($paymentList);
    }

    public function paymentDetail($paymentId)
    {
        $paymentDetails = $this->paypalSvc->getPaymentDetails($paymentId);

        dd($paymentDetails);
    }
}