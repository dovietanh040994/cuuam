<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Blog;
use App\Models\CategoryBlog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    protected $blog = '';
    protected $categoryBlog = '';

    public function __construct(Blog $blog, CategoryBlog $categoryBlog)
    {
        $this->blog = $blog;
        $this->categoryBlog = $categoryBlog;
    }

//    public function blog()
//    {
//        $showListBlog = $this->blog->where('b_status',1)->orderBy('id', 'desc')->paginate(8);
//        $dataView = [
//            'showListBlog' => $showListBlog,
//        ];
//        return view('frontend.blog',$dataView);
//    }

    public function blogDetail($id)
    {
        $postDetail = $this->blog->where('id', $id)->first();
//        dd($postDetail);
        $postHot = $this->blog->where('b_status', 1)->where('hot', 1)->get();
        $categoryBlogList = CategoryBlog::where('cpo_active', 1)->with('blog')->get();

        return view('frontend.postDetail', compact('postDetail', 'postHot', 'categoryBlogList'));
    }

    public function blogList()
    {
        $categoryBlogList = CategoryBlog::where('cpo_active', 1)->with(['blog' => function($query){
            $query->where('b_status', 1);
        }])->get();
        return view('frontend.postList', compact('categoryBlogList'));

    }

    public function blogCate($id)
    {
        $postHot = $this->blog->where('b_status', 1)->where('hot', 1)->get();
        if ($id == 0) {
            $postList = $this->blog->paginate(16);
            return view('frontend.post_list', compact('postList', 'postHot'));

        } else {
            $postList = $this->blog->where('b_category_id', $id)->paginate(16);
            $categoryBlog = $this->categoryBlog->where('id', $id)->first();
            return view('frontend.post_list', compact('postList', 'categoryBlog', 'postHot'));


        }
    }
}
