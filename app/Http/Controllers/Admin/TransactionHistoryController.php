<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Models\CategoryProduct;
use App\Models\GiftSend;
use App\Models\GiftSendHistory;
use App\Models\LuckyNumberSendHistory;
use App\Models\Product;
use App\Models\TimeSetting;
use App\Models\TransactionHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class TransactionHistoryController extends Controller
{
    public function index(Request $request)
    {
        $showHistory = TransactionHistory::with('product');
        $total = TransactionHistory::orderBy('id', 'desc');
        if ($request->type) {
            $showHistory = $showHistory->where('type', $request->type);
            $total = $total->where('type', $request->type);
        }
        if ($request->name) {
            $user = User::where('userid', $request->name)->value('id');
            $showHistory = $showHistory->where('user_id', $user);
            $total = $total->where('user_id', $user);
        }
        $total = $total->sum('coin');

        $showHistory = $showHistory->with('user')->with('userAdmin')->orderBy('id', 'desc')->paginate(10);
        $dataView = [
            'showHistory' => $showHistory,
            'total' => $total,
            'query' => $request->query()
        ];
        return view('admin.transactionHistory.index', $dataView);
    }

    public function add()
    {

        $user = User::get();
        return view('admin.transactionHistory.add', compact('user'));
    }

    public function store(Request $request)
    {

        $coin = $request->coin;
        $totalCoin = (int)$coin;
        $timeSetting = TimeSetting::where('id', 1)->where('status', 1)->first();
        if ($timeSetting) {
            if (Carbon::now() > Carbon::parse($timeSetting->start_time) && Carbon::now() < Carbon::parse($timeSetting->end_time)) {
                $totalCoin = (int)($coin * 2);
            }
        }
        $dataImport1 = [
            'user_id' => $request->user,
            'coin' => $totalCoin,
            'type' => 3,
            'admin_id' => Auth::guard('web')->user()->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ];
        TransactionHistory::insert($dataImport1);

        User::where('id', $request->user)->increment('coin', (int)$totalCoin);

//        dd($request->all(), $listUserNap->transactionHistory, $total);

        $listUserNap = User::where('id', $request->user)->with(['transactionHistory' => function ($query) {
            $query->whereIn('type', [1, 3]);
        }])->first();
        $total = 0;
        foreach ($listUserNap->transactionHistory as $value) {
            $total += $value->coin;
        }

        if ($total >= 100) {
            $product = GiftSend::where('giftCoin', 100)->first();
            $check = LuckyNumberSendHistory::where('user_id', $request->user)->where('gift', 100)->first();
            if (!$check) {
                $dataImport = [
                    'turn' => $product->luckyNumber,
                    'gift' => 100,
                    'user_id' => $request->user,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
                LuckyNumberSendHistory::insert($dataImport);

                User::where('id', $request->user)->increment('luckyNumber', (int)$product->luckyNumber);
            }
        }
        if ($total >= 200) {
            $product = GiftSend::where('giftCoin', 200)->first();
            $check = LuckyNumberSendHistory::where('user_id', $request->user)->where('gift', 200)->first();
            if (!$check) {
                $dataImport = [
                    'turn' => $product->luckyNumber,
                    'gift' => 200,
                    'user_id' => $request->user,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
                LuckyNumberSendHistory::insert($dataImport);
                User::where('id', $request->user)->increment('luckyNumber', (int)$product->luckyNumber);

            }
        }
        if ($total >= 500) {
            $product = GiftSend::where('giftCoin', 500)->first();
            $check = LuckyNumberSendHistory::where('user_id', $request->user)->where('gift', 500)->first();
            if (!$check) {
                $dataImport = [
                    'turn' => $product->luckyNumber,
                    'gift' => 500,
                    'user_id' => $request->user,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
                LuckyNumberSendHistory::insert($dataImport);
                User::where('id', $request->user)->increment('luckyNumber', (int)$product->luckyNumber);

            }
        }
        if ($total >= 1000) {
            $product = GiftSend::where('giftCoin', 1000)->first();
            $check = LuckyNumberSendHistory::where('user_id', $request->user)->where('gift', 1000)->first();
            if (!$check) {
                $dataImport = [
                    'turn' => $product->luckyNumber,
                    'gift' => 1000,
                    'user_id' => $request->user,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
                LuckyNumberSendHistory::insert($dataImport);
                User::where('id', $request->user)->increment('luckyNumber', (int)$product->luckyNumber);

            }
        }
        if ($total >= 2000) {
            $product = GiftSend::where('giftCoin', 2000)->first();
            $check = LuckyNumberSendHistory::where('user_id', $request->user)->where('gift', 2000)->first();
            if (!$check) {
                $dataImport = [
                    'turn' => $product->luckyNumber,
                    'gift' => 2000,
                    'user_id' => $request->user,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
                LuckyNumberSendHistory::insert($dataImport);
                User::where('id', $request->user)->increment('luckyNumber', (int)$product->luckyNumber);

            }
        }
        if ($total >= 5000) {
            $product = GiftSend::where('giftCoin', 5000)->first();
            $check = LuckyNumberSendHistory::where('user_id', $request->user)->where('gift', 5000)->first();
            if (!$check) {
                $dataImport = [
                    'turn' => $product->luckyNumber,
                    'gift' => 5000,
                    'user_id' => $request->user,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
                LuckyNumberSendHistory::insert($dataImport);
                User::where('id', $request->user)->increment('luckyNumber', (int)$product->luckyNumber);

            }
        }
        if ($total >= 10000) {
            $product = GiftSend::where('giftCoin', 10000)->first();
            $check = LuckyNumberSendHistory::where('user_id', $request->user)->where('gift', 10000)->first();
            if (!$check) {
                $dataImport = [
                    'turn' => $product->luckyNumber,
                    'gift' => 10000,
                    'user_id' => $request->user,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
                LuckyNumberSendHistory::insert($dataImport);
                User::where('id', $request->user)->increment('luckyNumber', (int)$product->luckyNumber);

            }
        }


        return redirect()->route('admin.transactionHistory.index')->with('success', ' Thêm coin thành công !!! ');
    }

    public function edit()
    {

        $user = User::get();
        return view('admin.transactionHistory.edit', compact('user'));
    }

    public function storeMinus(Request $request)
    {
        $dataImport = [
            'user_id' => $request->user,
            'coin' => $request->coin,
            'type' => 4,
            'admin_id' => Auth::guard('web')->user()->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ];
        $id = TransactionHistory::insert($dataImport);
        $user = User::where('id', $request->user)->value('coin');
        if ((int)$request->coin > (int)$user) {
            return redirect()->back()->with(['alert' => 'warning', 'message' => 'Tài khoản không đủ coin']);

        }
        User::where('id', $request->user)->decrement('coin', (int)$request->coin);
        if ($id > 0) {
            return redirect()->route('admin.transactionHistory.index')->with('success', ' Trừ coin thành công !!! ');
        }
        return redirect()->route('admin.transactionHistory.index')->with('danger', ' Trừ coin thất bại !!! ');
    }

}
