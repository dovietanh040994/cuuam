<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Models\CategoryProduct;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $showproduct = Product::where('prd_name','like','%'.$request->name.'%')
            ->where('prd_category_product_id','like','%'.$request->category_product.'%')
            ->with(['categoryProduct'=>function ($query){
                $query->where('cpr_active',1);
            }])->orderBy('id','desc')->paginate(10);
        $category = CategoryProduct::where('cpr_active',1)->get();

        $dataView = [
            'showproduct' => $showproduct,
            'category' => $category,
            'query'       => $request->query()
        ];
        return view('admin.product.index',$dataView);
    }

    public function add()
    {
        $category = CategoryProduct::where('cpr_active',1)->get();
        return view('admin.product.add',compact('category'));
    }

    public function store(ProductRequest $request)
    {
        $data = $request->all();
        if ($request->hasFile('prd_thunbar'))
        {
            $info = uploadImg('prd_thunbar');
            if($info['code'] == 1)
            {
                $data['prd_thunbar'] = $info['name'];
                move_uploaded_file($_FILES['prd_thunbar']['tmp_name'], public_path() . '/uploads/imgProduct/' . $info['name']);
            }
        }
        $data['prd_slug'] = str_slug($request->prd_name);
        $data['created_at'] = $data['updated_at'] = Carbon::now();
        unset($data['_token']);
        $id = Product::insert($data);
        if($id > 0)
        {
            return redirect()->route('admin.product.index')->with('success',' The update was successful !!! ');
        }
        return redirect()->route('admin.product.index')->with('danger',' The update was failed !!! ');
    }

    public function edit(Request $request,$id)
    {
        $category = CategoryProduct::where('cpr_active',1)->get();
        $product = Product::findOrFail($id);
        return view('admin.product.edit',compact('product','category'));
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();
//        dd($data);
        unset($data['_token']);

        if ($request->hasFile('prd_thunbar'))
        {

            $info = uploadImg('prd_thunbar');
            if($info['code'] == 1)
            {
                $data['prd_thunbar'] = $info['name'];
                move_uploaded_file($_FILES['prd_thunbar']['tmp_name'], public_path() . '/uploads/imgProduct/' . $info['name']);
            }

        }else{
            $data['prd_thunbar'] = Product::findOrFail($id)->prd_thunbar;

        }
        $data['prd_slug'] = str_slug($request->prd_name);
        $data['updated_at'] = Carbon::now();
        $id = Product::where('id',$id)->update($data);
        if($id > 0)
        {
            return redirect()->route('admin.product.index')->with('success',' The update was successful !!! ');
        }
        return redirect()->route('admin.product.index')->with('danger',' The update was failed !!! ');
    }

    public function delete($id)
    {
        $product = Product::findOrFail($id);
        $flagCheck = $product->delete();
        if($flagCheck > 0)
        {
            return redirect()->back()->with('success',' The update was successful !!! ');
        }
        return redirect()->back()->with('danger',' The update was failed !!! ');
    }
    public function status($id,$status)
    {
        if($status == 1){
            Product::where('id',$id)->update(['prd_status' => 0]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }elseif($status == 0){
            Product::where('id',$id)->update(['prd_status' => 1]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }
        return redirect()->back()->with('danger','  The update was failed !!! ');

    }
    public function luckyStatus($id,$status)
    {
        if($status == 1){
            Product::where('id',$id)->update(['luckyStatus' => 2]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }elseif($status == 2){
            Product::where('id',$id)->update(['luckyStatus' => 1]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }
        return redirect()->back()->with('danger','  The update was failed !!! ');

    }
    public function hot($id,$hot)
    {
        if($hot == 1){
            Product::where('id',$id)->update(['prd_hot' => 0]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }elseif($hot == 0){
            Product::where('id',$id)->update(['prd_hot' => 1]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }
        return redirect()->back()->with('danger','  The update was failed !!! ');

    }

    public function active($id,$active)
    {
        if($active == 1){
            Product::where('id',$id)->update(['prd_active' => 0]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }elseif($active == 0){
            Product::where('id',$id)->update(['prd_active' => 1]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }
        return redirect()->back()->with('danger','  The update was failed !!! ');

    }
}
