<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoryBlog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use Carbon\Carbon;
class BlogController extends Controller
{
    public function index(Request $request)
    {
        $showBlog = Blog::where('b_name','like','%'.$request->name.'%')
            ->where('b_category_id','like','%'.$request->category_blog.'%')
            ->with(['categoryBlog'=>function ($query){
                $query->where('cpo_active',1);
        }])->orderBy('id','desc')->paginate(10);
        $category = CategoryBlog::where('cpo_active',1)->get();
        $dataView = [
            'showBlog' => $showBlog,
            'category' => $category,
            'query'       => $request->query()
        ];
        return view('admin.blog.index',$dataView);
    }

    public function add()
    {
        $category = CategoryBlog::where('cpo_active',1)->get();
        return view('admin.blog.add',compact('category'));
    }

    public function store(BlogRequest $request)
    {
        $data = $request->all();
        if ($request->hasFile('b_thunbar'))
        {
            $info = uploadImg('b_thunbar');
            if($info['code'] == 1)
            {
                $data['b_thunbar'] = $info['name'];
                move_uploaded_file($_FILES['b_thunbar']['tmp_name'], public_path() . '/uploads/imgBlog/' . $info['name']);
            }
        }
        $data['b_slug'] = str_slug($request->b_name);
        $data['created_at'] = $data['updated_at'] = Carbon::now();
        unset($data['_token']);
        unset($data['files']);

        $id = Blog::insert($data);
        if($id > 0)
        {
            return redirect()->route('admin.blogs.index')->with('success',' The update was successful !!! ');
        }
        return redirect()->route('admin.blogs.index')->with('danger',' The update was failed !!! ');
    }

    public function edit(Request $request,$id)
    {
        $category = CategoryBlog::where('cpo_active',1)->get();
        $blog = Blog::findOrFail($id);
        return view('admin.blog.edit',compact('blog','category'));
    }

    public function update(Request $request,$id)
    {

        $data = $request->all();

        if ($request->hasFile('b_thunbar'))
        {
            $info = uploadImg('b_thunbar');
            if($info['code'] == 1)
            {
                $data['b_thunbar'] = $info['name'];
                move_uploaded_file($_FILES['b_thunbar']['tmp_name'], public_path() . '/uploads/imgBlog/' . $info['name']);
            }

        }else{
            $data['b_thunbar'] = Blog::findOrFail($id)->b_thunbar;
        }
        $data['b_slug'] = str_slug($request->b_name);
        $data['updated_at'] = Carbon::now();
        unset($data['files']);
        unset($data['_token']);

        $id = Blog::where('id',$id)->update($data);
        if($id > 0)
        {
            return redirect()->route('admin.blogs.index')->with('success',' The update was successful !!! ');
        }
        return redirect()->route('admin.blogs.index')->with('danger',' The update was failed !!! ');
    }

    public function delete($id)
    {
        $blog = Blog::findOrFail($id);
        $flagCheck = $blog->delete();
        if($flagCheck > 0)
        {
            return redirect()->back()->with('success',' The update was successful !!! ');
        }
        return redirect()->back()->with('danger',' The update was failed !!! ');
    }
    public function status($id,$status)
    {
        if($status == 1){
             Blog::where('id',$id)->update(['b_status' => 0]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }elseif($status == 0){
            Blog::where('id',$id)->update(['b_status' => 1]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }
        return redirect()->back()->with('danger','  The update was failed !!! ');

    }
    public function hot($id,$hot)
    {
//        dd($id,$hot);
        if($hot == 1){
             Blog::where('id',$id)->update(['hot' => 0]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }elseif($hot == 0){
            Blog::where('id',$id)->update(['hot' => 1]);
            return redirect()->back()->with('success',' The update was successful !!! ');
        }
        return redirect()->back()->with('danger','  The update was failed !!! ');

    }
}
