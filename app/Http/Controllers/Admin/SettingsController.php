<?php

namespace App\Http\Controllers\Admin;

use App\Models\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function index()
    {
        $settingOptions = Settings::select('option_key', 'option_value')->get()->pluck('option_value', 'option_key');
        return view('admin.settings.index',compact('settingOptions'));
    }

    public function update(Request $request)
    {

        $data = $request->except('_token');
//        if ($request->hasFile('logo'))
//        {
//            $info = uploadImg('logo');
//            if($info['code'] == 1)
//            {
//                $data['logo'] = $info['name'];
//                move_uploaded_file($_FILES['logo']['tmp_name'], public_path() . '/uploads/logo/' . $info['name']);
//            }
//
//        }else{
//            $data['logo'] = Settings::where('option_key','logo')->first()->option_value;
//        }
//dd($data);
        foreach($data as $key => $value){
            $data = Settings::where('option_key', $key)->first();
            $data->option_value = $value;
            $data->save();
        }
        return redirect()->back()->with('success',' The update was successful !!! ');
    }
}
