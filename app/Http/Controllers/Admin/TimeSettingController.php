<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BlogRequest;
use App\Http\Requests\GiftCodeRequest;
use App\Http\Requests\ProductRequest;
use App\Models\Blog;
use App\Models\CategoryBlog;
use App\Models\CategoryProduct;
use App\Models\GiftCode;
use App\Models\NameGiftCode;
use App\Models\Product;
use App\Models\TimeSetting;
use App\Models\TransactionHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TimeSettingController extends Controller
{

    public function index(Request $request)
    {

        $setting = TimeSetting::get();
        return view('admin.timeSetting.index', compact('setting'));
    }


    public function add(Request $request)
    {
        return view('admin.timeSetting.add');
    }

    public function store(Request $request)
    {
        $data = $request->except('_token');

        $dataImport = [
            'start_time' => Carbon::parse($data['start_time']),
            'end_time' => Carbon::parse($data['end_time']),
            'status' => $data['status'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ];

        $id = TimeSetting::insert($dataImport);
        if ($id > 0) {
            return redirect()->route('admin.timeSetting.index')->with('success', ' The update was successful !!! ');
        }
        return redirect()->route('admin.timeSetting.index')->with('danger', ' The update was failed !!! ');
    }

    public function edit(Request $request, $id)
    {
        $blog = TimeSetting::findOrFail($id);
        $startDate = explode(' ', $blog->start_time)[0];
        $endDate = explode(' ', $blog->end_time)[0];
        $number = $blog->number;

        return view('admin.timeSetting.edit', compact('blog', 'startDate', 'endDate', 'number'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->except('_token');

        $dataImport = [
            'start_time' => Carbon::parse($data['start_time']),
            'end_time' => Carbon::parse($data['end_time']),
            'number' => $data['number'],
            'status' => $data['status'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ];

        $id = TimeSetting::where('id', $id)->update($dataImport);
        if ($id > 0) {
            return redirect()->route('admin.timeSetting.index')->with('success', ' The update was successful !!! ');
        }
        return redirect()->route('admin.timeSetting.index')->with('danger', ' The update was failed !!! ');
    }


}
