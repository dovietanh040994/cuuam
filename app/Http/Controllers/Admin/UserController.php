<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

;

use Auth;

class UserController extends Controller
{
    public function getLogin()
    {
        return view('admin.user.login');
    }

    public function postLogin(Request $request)
    {
        $rules = [
            'user' => 'required',
            'password' => 'required'
        ];
        $messages = [
            'user.required' => 'User không được để trống',
            'password.required' => 'Password không được để trống',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user = User::where('userid', $request->user)
                ->where('userpassword', md5($request->password))
                ->first();

            if ($user) {
                if ($user->admin !== 1) {
                    $errors = new MessageBag(['errorlogin' => 'Tài khoản không có quyền admin']);
                    return redirect()->back()->withInput()->withErrors($errors);
                }
                Auth::login($user, true);
                return redirect()->intended('admins');
            } else {
                $errors = new MessageBag(['errorlogin' => 'The email or password is incorrect']);
                return redirect()->back()->withInput()->withErrors($errors);
            }
        }


    }

    public function getLogout()
    {
        Auth::logout();
        return redirect('loginAdmin');
    }
}
