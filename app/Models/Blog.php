<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog';

    public function categoryBlog()
    {
        return $this->belongsTo(CategoryBlog::class,'b_category_id','id');
    }

    public function convertSlug($slug)
    {
        $pattern = '/(?<=-)(\d+)$/i';
        preg_match($pattern, $slug, $match);

        if (isset($match[1]))
        {
            $id = $match[1];
            return $id;
        }
    }
}
