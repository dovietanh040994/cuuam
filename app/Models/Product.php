<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function categoryProduct()
    {
        return $this->belongsTo(CategoryProduct::class,'prd_category_product_id','id');
    }
}
