<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Tbl_Cash_Inven extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'cuuamsql';
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Tbl_Cash_Inven';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['item_code','item_user_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '',
    ];

//    public function causer()
//    {
//        return $this->belongsTo(User::class);
//    }
}
